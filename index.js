const btnGet = document.querySelector(".btn__get"),
  btnSticky = document.querySelector(".btn_sticky"),
  theadSticky = document.querySelector(".table_thead");

function showModal(btn /*tableHead*/) {
  btn.addEventListener("click", (e) => {
    console.log(e);
  });
}

function scroll(btn) {
  btn.classList.add("btn_sticky__scroll");
  tableHead.classList.add("table_thead__sticky");
}

window.onload = function checkWindowProp() {
  if (window.scrollY > 10) scroll(btnSticky, theadSticky);

  window.addEventListener("scroll", (e) => {
    // console.log(e);
    if (e.path[1].scrollY > 59) {
      scroll(btnSticky, theadSticky);
    } else {
      btnSticky.classList.remove("btn_sticky__scroll");
      theadSticky.classList.remove("table_thead__sticky");
    }
  });
};

showModal(btnGet);
